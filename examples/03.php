<?php
    require_once("../class/db.class.php");
    $db = new db('root', '', 'test', 'localhost');
    
    // genera una cadena aleatoria de caracteres capitalizada
    function generateRandomString($length = 8){
        $characters = 'abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for($i = 0; $i < $length; $i++){
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        
        return ucfirst($randomString);
    }
    
    $sql = "INSERT INTO customers(name, lastname) VALUES ('" . generateRandomString() . "', '" . generateRandomString() . "');";
    
    var_dump($db->sql($sql));