<?php
    require_once("../class/db.class.php");
    $db = new db('root', '', 'test', 'localhost');
    
    $sql = "SELECT * FROM customers;";
    $result = $db->sql($sql);
    
    // control simple
    if($result['ok']){
        foreach($result['data'] as $key => $value){
            echo "Id: " . $value['id'] . " | Name: " . $value['lastname'] . ", " . $value['name'] . "<br/>";
        }
        echo "<hr>";
        echo "Cantidad de registros: " . $db->getCount() . "<br />";
        echo "Primer registro -> " . $db->getFirst()['id'] . " - " . $db->getFirst()['lastname'] . " - " . $db->getFirst()['name'] . "<br />";
        echo "Último registro -> " . $db->getLast()['id'] . " - " . $db->getLast()['lastname'] . " - " . $db->getLast()['name'] . "<br />";
    }else{
        echo "Ocurrio un error en la consulta.";
    }