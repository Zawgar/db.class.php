<?php
    require_once("../class/db.class.php");
    $db = new db('root', '', 'test', 'localhost');
    
    if($db->testConnection()){
        echo "La conexión es correcta.<br>";
    }else{
        echo "No se puede establecera la conexíon.";
        exit;
    }
    
    if($db->existTable("customers")){
        echo "La tabla SI existe.";
    }else{
        echo "La tabla NO existe.";
    }
    
    $sql = "DROP TABLE IF EXISTS `customers`;
            CREATE TABLE `customers` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(128) CHARACTER SET latin1 NOT NULL,
                `lastname` VARCHAR(128) CHARACTER SET latin1 NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
    
    var_dump($db->sql($sql));