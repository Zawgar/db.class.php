# DB.class.php

### Descripción general
Crea la conexión a la Base de Datos y ejecuta una o mas consultas sql utilizando MySqli. Utiliza transacciones para ejecutar las consultas. Captura los errores a distintos niveles y devuelve un detalle si es requerido.

### Ideología
El objetivo es crear una clase que se encargue de administrar la conexión con la base de datos y ejecutar consultas sql, devolviendo únicamente, un valor `booleando` o un `array` con los resultados, listo para utilizar en cualquier aplicación.

Todos los errores deben ser capturados por la clase y no interrumpir la continuidad de la aplicación.

El driver utilizado por defecto es MySqli.

## Requeriminetos
- PHP > 5.4.0

## Intancia
### Parámetros
* Usuario (`string`)
* Contraseña (`string`)
* Nombre de la Base de datos (`string`)
* Host (`string`/default: _"localhost"_)
* Puerto (`string`/default: _"3306"_)
* Codificación (`string`/default: _"utf8"_)

### Ejemplo
```php
require_once('class/db.class.php');
$db = new db('user', 'pass', 'host', 'db', '3306', 'utf8');
```

## Consulta
### Parámetros
El método "sql" acepta los siguientes parámetros:
* cadenaSql (`string`): Una o mas consultas sql separadas con ";". Si la consulta es única, es indistinto el ";" final.
* detalle (`bool`/defualt: **FALSE**): devuelve un array con los detalle de los errores obtenidos
* json (`bool`/defualt: **FALSE**): especifica si devuelve una cadena json del resultado obtenido

### Valores devueltos
#### Consulta única
El método "sql" devuelve un array con las siguientes claves:
* 'ok' (bool): dependiendo del resultado de la consulta
* 'data' (array): array con el resultado de una consulta "SELECT" o "SHOW", caso contrario, se encontrará vacío
* 'summary' (string/opcional): detalle de la ejecición de la consulta

#### Consultas múltiples
El método "sql" devuelve un array con las siguientes claves:
* 'ok' (bool): devuelve **TRUE** solamente si todas las consultas se ejecutaron correctamente
* 'data' (array): vacío
* 'summary' (string/opcional): detalle de la ejecición de la consulta

### Ejemplo
```php
$sql = "INSERT INTO clients(name, lasname) VALUES ('Rmwewhnt', 'Krljyxccuxko');";
$sql .= "SELECT * FROM clients";
$db->sql($sql);
```

### Métodos
#### getCount()
Devuelve la cantidad de registros obtenidos en el último SELECT.
#### getFirst()
Devuelve el primer registro del último SELECT.
#### getLast()
Devuelve el último registro del último SELECT.

### Pendientes
- manejar resultados de STORE PROCEDURES
- agregar mas métodos para devolver distintos valores
- pensar en soportar consultas directamente por AJAX
- debería poder cambiarse el driver (MySqli) que utiliza para conectarse.
(http://php.net/manual/es/mysqlinfo.api.choosing.php)
- agregar MongoDB para loguear las transacciones

##### Algunas ideas sacadas de otros lugares
- https://github.com/joshcam/PHP-MySQLi-Database-Class/blob/master/MysqliDb.php
