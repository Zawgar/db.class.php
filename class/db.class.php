<?php
    /*
    developed by Zawgar
    https://Zawgar@bitbucket.org/Zawgar/db.class.php.git
    */
    if(!class_exists('Db')){
        class Db{

            // conexión a la DB
            public $conecction;

            // array con todas las cosultas separadas
            public $sqlArray;

            // propiedad de uso interno para almacenar errores y detalles de los mismos
            private $result = [
                'ok'        => true,
                "summary"   => [],
                "data"      => []
            ];

            // propiedades de conexión y configuración a la base de datos
            private $user;
            private $pass;
            private $database;
            private $host;
            private $port;
            private $charset;

            private $returnSummary = false;
            private $returnFormat = "array";
            
            // Almacena la cantidad de filas devueltas en el último SELECT
            private $count;

            // Almacena los registros del primero y del último registro de la última consulta SELECT
            public $arraySelectFirstLast = [
                "first" => null,
                "last"  => null
            ];
            
            /**
             * Db constructor.
             *
             * @param string $user      nombre del usuario que se conecta a la DB
             * @param string $pass      contraseña del usuario que se conecta a la DB
             * @param string $database   nombre de la DB
             * @param string $host      (default: "localhost") nombre o ip del host en el que se encuentra la DB
             * @param string $port      (default: "3306") numero del puerto al que se conecta
             * @param string $charset   (default: "utf8") codificacion que se utiliza en la conección
             */
            public function __construct(
                string $user        = "root",
                string $pass        = "",
                string $database    = "",
                string $host        = "localhost",
                string $port        = "3306",
                string $charset     = "utf8"
            ){
                $this->user     = $user;
                $this->pass     = $pass;
                $this->database = $database;
                $this->host     = $host;
                $this->port     = $port;
                $this->charset  = $charset;
                mb_internal_encoding($this->charset);
                mb_regex_encoding($this->charset);
                mysqli_report(MYSQLI_REPORT_STRICT);
            }

            // abre la conexión
            protected function connect(){
                // intenta conectar con la DB y detecta si existe algun error
                try{
                    $this->conecction = mysqli_connect($this->host, $this->user, $this->pass, $this->database);
                }catch(Exception $e){
                    $this->result['ok'] = false;
                    $this->addSummary("Error al crear la conexión a la base de datos. [" . $e . "]");
                    return false;
                }

                // detecta si existió algún error en el intento conecta con la DB
                if($this->conecction->connect_errno > 0){
                    $this->result['ok'] = false;
                    $this->addSummary("Error en la conexión a la base de datos. [" . $this->conecction->connect_error . "]");
                }

                // detecta si hay error al setear el conjunto de caracteres
                if(!$this->conecction->set_charset($this->charset)){
                    $this->result['ok'] = false;
                    $this->addSummary("Error cargando el conjunto de caracteres utf8. [" . $this->conecction->error . "]");
                }

                return $this->result['ok'];
            }

            /**
             * cierra la conexión
             */
            protected function close(){
                $this->conecction->close();
            }
    
            /**
             * ejecuta la/s consulta/s SQL
             *
             * @param string $sqlString     texto de la consulta. puede contener multiples consultas separadas por ";"
             * @param bool $showSummary     (default: FALSE) -> especifica si devuelve o no un detalle de la cosulta enviada. solamente funciona si se envía una única consulta
             * @param string $format        (default: 'array') -> especifica si se quiere obtener como resultado una cadena de texto con formato JSON
             *
             * @return array|false|string
             *
             * Si se ejecuta correctamente, devuleve un array de resultados en el caso de que la consulta sea un SELECT o TRUE si es otro caso
             * En caso de error, devuelve FALSE
             */
            public function sql(string $sqlString = "", bool $showSummary = false, $format = "array"){
                $this->returnSummary = $showSummary;
                $this->returnFormat = $format;
                
                // conmpruebo que pueda utilizarse la extensión "mysqli" en el servidor
                if(!extension_loaded('mysqli')){
                    $this->result['ok'] = false;
                    $this->addSummary("La extensión MYSQLI no se encuentra instalada en el servidor.");

                    return $this->getResult();
                }

                // conectar con la base de datos y detecto si existió algun error en el proceso
                if(!$this->connect()){
                    $this->result['ok'] = false;
                    $this->addSummary("Error en la conexión con la base de datos.");

                    return $this->getResult();
                }

                // normalizo y limpio la cadena
                // crea un array con cada una de las consultas en una propiedad
                $this->sqlNormalize($sqlString);

                // Habilito el autocommit
                $this->conecction->autocommit(false);

                // ejecución de la/s transacción/es
                try{
                    foreach($this->sqlArray as $key => $value){
                        $result = $this->conecction->query($value['sql']);
                        if($result === false){
                            $this->result['ok'] = false; // en caso de error de alguna de las consultas
                            $this->addSummary("Error en la ejecución de la consulta. [" . $this->conecction->error . "]");
                        }
                    }
                }catch(Exception $e){
                    $this->conecction->rollback();
                    $this->addSummary("Error en la ejecución de la consulta. [" . $e->getMessage() . "]");
                    $this->result['ok'] = false;
                }

                // control de errores de la/s consulta/s
                if($this->result['ok']){
                    $this->conecction->commit();
                }else{
                    $this->conecction->rollback();
                }

                // Des-Habilito el autocommit
                $this->conecction->autocommit(true);

                // Armo la respuesta de la consulta
                if($this->result['ok']){
                    if(count($this->sqlArray) === 1){
                        switch($this->sqlArray[0]['type']){
                            case "select":
                            case "show":
                                // completa el array con el resultado de la consulta
                                while($row = mysqli_fetch_assoc($result)){
                                    $this->result['data'][] = $row;
                                }

                                // Guardo la cantidad de registros devueltos
                                $this->count = count($this->result['data']);

                                // Guardo el primer y el último registro
                                if($this->count > 0){
                                    $this->arraySelectFirstLast['first'] = $this->result['data'][0];
                                    $this->arraySelectFirstLast['last'] = $this->result['data'][(count($this->result['data']) - 1)];
                                }
                                break;
                        }
                    }else{
                        $this->arraySelectFirstLast = ["first" => false, "last" => false];
                    }
                }

                // Cierro la conexión a la DB
                $this->close();

                // Resultado de la consulta
                return $this->getResult();
            }

            // devuelve la primer palabra (tipo) de la consulta SQL en minúculas
            protected function sqlType(string $string = ""){
                $arrayTemp = explode(" ", $string);
                $typeReturn = strtolower(trim($arrayTemp[0]));

                return $typeReturn;
            }

            // procedimiento para limpiar la cadena de texto de la consulta
            private function sqlClear(string $sqlString = ""){
                $sqlString = preg_replace('/\s+/', ' ', trim($sqlString));

                if(!mb_detect_encoding($sqlString, 'UTF-8', true)){
                    $sqlString = utf8_encode($sqlString);
                }
                return $sqlString;
            }

            // normalizo la estructura de la consulta
            // devuelve un array con todas las consultas
            private function sqlNormalize(string $sqlString = ""){
                if(substr($sqlString, -1) == ";"){
                    $sqlString = substr($sqlString, 0, -1);
                }
                foreach(explode(";", $sqlString) as $key => $value){
                    $this->sqlArray[$key]['type'] = $this->sqlType($value);
                    $this->sqlArray[$key]['sql'] = $this->sqlClear($value);
                }
            }

            // limpia un valor específico
            public function clearValue(string $value = ""){
                $valueTemp = trim($value);
                $delCharacters = ["'", "\"", "`"];
                if(in_array(substr($valueTemp, 0, 1), $delCharacters)){
                    $valueTemp = substr($valueTemp, 1);
                }
                if(in_array(substr($valueTemp, -1), $delCharacters)){
                    $valueTemp = substr($valueTemp, 0, -1);
                }

                // Si es un valor numérico, no agrega las comillas
                if(is_numeric($valueTemp)){
                    return $this->conecction->real_escape_string(trim($valueTemp));
                }else{
                    return "'" . $this->conecction->real_escape_string(trim($valueTemp)) . "'";
                }
            }

            // devuelve el resultado de la consulta
            private function getResult(){
                $arrTemp = [
                    'ok'    => $this->result['ok'],
                    'data'  => $this->result['data']
                ];

                // devuelve o no el detalle
                if($this->returnSummary) $arrTemp['summary'] = $this->result['summary'];

                // Devuelvo el resutlado en JSON si es solicitado
                switch($this->returnFormat){
                    case "json":
                        return json_encode($arrTemp);
                        break;
                        
                    default:
                    case "array":
                        return $arrTemp;
                        break;
                }
            }

            // devuelve el primer registro del último SELECT
            // return: array
            public function getFirst(){
                return $this->arraySelectFirstLast['first'];
            }

            // devuelve el último registro del último SELECT
            // return: array
            public function getLast(){
                return $this->arraySelectFirstLast['last'];
            }

            // devuelve la cantidad de registros obtenidos en el último SELECT
            // return: int
            public function getCount(){
                return $this->count;
            }

            // comprueba si se puede realizar la conexión con la db
            // return: true/false
            public function testConnection(){
                if($result = $this->connect()){
                    $this->close();
                }

                return $result;
            }

            // comprueba si existe la tabla en la db actual
            // return: true/false
            public function existTable(string $table = ""){
                $this->connect();
                $this->sql("SHOW TABLES LIKE '" . $table . "';");
                if($this->getCount() > 0) return true;

                $this->addSummary("La tabla '" . $table . "'' no existe en la base de datos");
                return false;
            }

            private function addSummary($text = ""){
                $this->result["summary"][] = $text;
            }
            
        }
    }
    